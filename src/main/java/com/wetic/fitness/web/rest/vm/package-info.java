/**
 * View Models used by Spring MVC REST controllers.
 */
package com.wetic.fitness.web.rest.vm;
